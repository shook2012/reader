package com.simemg.reader.provider;

import com.simemg.reader.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;
import java.util.Optional;

public class BookNameListProvider extends BaseItemProvider {
    private final List<String> bookNameList;
    private final Context context;

    public BookNameListProvider(List<String> bookNameList, Context context) {
        this.bookNameList = bookNameList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return bookNameList == null ? 0 : bookNameList.size();
    }

    @Override
    public Object getItem(int i) {
        return  Optional.of(bookNameList.get(i));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_component_book_name, null, false);
        if (!(component instanceof ComponentContainer)) {
            return null;
        }
        if (bookNameList.get(i) != null) {
            Text text = (Text) component.findComponentById(ResourceTable.Id_text);
            text.setText(bookNameList.get(i));
        }
        return component;
    }
}
