package com.simemg.reader.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * LogUtil
 */
public class LogUtil {
    private static final String TAG_LOG = "LogUtil";
    private static final int DOMAIN_ID = 0xD000F00;
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, DOMAIN_ID, LogUtil.TAG_LOG);

    private LogUtil() {
    }

    public static void debug(String msg) {
        HiLog.debug(LABEL_LOG, msg);
    }

    public static void debug(int msg) {
        HiLog.debug(LABEL_LOG, String.format("int: %s", msg));
    }

    public static void debug(Object obj) {
        HiLog.debug(LABEL_LOG, String.format("obj: %s", obj.toString()));
    }

    public static void info(String msg) {
        HiLog.info(LABEL_LOG, msg);
    }

    public static void info(int msg) {
        HiLog.info(LABEL_LOG, String.format("int: %s", msg));
    }

    public static void info(Object obj) {
        HiLog.info(LABEL_LOG, String.format("obj: %s", obj.toString()));
    }
}
