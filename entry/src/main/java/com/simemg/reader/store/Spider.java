package com.simemg.reader.store;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

@Entity(tableName = "spider")
public class Spider extends OrmObject {
    @PrimaryKey(autoGenerate = true)
    private Integer SpiderID;
    private String HostUrl;
    private String SearchUrl;
    private String BookSelect;
    private String IndexUrlSelect;
    private String NameSelect;
    private String AuthorSelect;
    private String ImgSelect;
    private String InfoSelect;
    private String LastPageSelect;
    private String PageSelect;
    private String ContentSelect;
    private String InrInfoSelect;
    private String InrImgSelect;

    public Integer getSpiderID() {
        return SpiderID;
    }

    public void setSpiderID(Integer spiderID) {
        SpiderID = spiderID;
    }

    public String getHostUrl() {
        return HostUrl;
    }

    public void setHostUrl(String hostUrl) {
        HostUrl = hostUrl;
    }

    public String getSearchUrl() {
        return SearchUrl;
    }

    public void setSearchUrl(String searchUrl) {
        SearchUrl = searchUrl;
    }

    public String getBookSelect() {
        return BookSelect;
    }

    public void setBookSelect(String bookSelect) {
        BookSelect = bookSelect;
    }

    public String getIndexUrlSelect() {
        return IndexUrlSelect;
    }

    public void setIndexUrlSelect(String indexUrlSelect) {
        IndexUrlSelect = indexUrlSelect;
    }

    public String getNameSelect() {
        return NameSelect;
    }

    public void setNameSelect(String nameSelect) {
        NameSelect = nameSelect;
    }

    public String getAuthorSelect() {
        return AuthorSelect;
    }

    public void setAuthorSelect(String authorSelect) {
        AuthorSelect = authorSelect;
    }

    public String getImgSelect() {
        return ImgSelect;
    }

    public void setImgSelect(String imgSelect) {
        ImgSelect = imgSelect;
    }

    public String getInfoSelect() {
        return InfoSelect;
    }

    public void setInfoSelect(String infoSelect) {
        InfoSelect = infoSelect;
    }

    public String getLastPageSelect() {
        return LastPageSelect;
    }

    public void setLastPageSelect(String lastPageSelect) {
        LastPageSelect = lastPageSelect;
    }

    public String getPageSelect() {
        return PageSelect;
    }

    public void setPageSelect(String pageSelect) {
        PageSelect = pageSelect;
    }

    public String getContentSelect() {
        return ContentSelect;
    }

    public void setContentSelect(String contentSelect) {
        ContentSelect = contentSelect;
    }

    public String getInrInfoSelect() {
        return InrInfoSelect;
    }

    public void setInrInfoSelect(String inrInfoSelect) {
        InrInfoSelect = inrInfoSelect;
    }

    public String getInrImgSelect() {
        return InrImgSelect;
    }

    public void setInrImgSelect(String inrImgSelect) {
        InrImgSelect = inrImgSelect;
    }


}
