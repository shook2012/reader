package com.simemg.reader.provider;

import com.simemg.reader.ResourceTable;
import com.simemg.reader.store.Book;
import com.simemg.reader.utils.RouterUtil;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RankingListProvider extends BaseItemProvider {

    public static class ListItem {
        private boolean display = false;

        private final List<Book> tempList = new ArrayList<>();
        private final List<Book> bookList;
        private final String title;
        private boolean show = false;

        private Text text;
        private ListContainer list;
        private BaseItemProvider provider;
        private BaseItemProvider parentProvider;
        private Component component;
        private Ability ability;

        public ListItem(String title, List<Book> bookList) {
            this.title = title;
            this.bookList = bookList;
        }

        public Component getComponent(Ability ability) {
            this.ability = ability;
            component = LayoutScatter.getInstance(ability).parse(ResourceTable.Layout_component_stack_list, null, false);
            list = (ListContainer)component.findComponentById(ResourceTable.Id_list);
            text = (Text)component.findComponentById(ResourceTable.Id_title);

            text.setText(title);
            provider = new BookListProvider(tempList, ability);
            list.setItemProvider(provider);
            provider.notifyDataChanged();
            display = true;
            return component;
        }

        public void addComponentListen() {
            text.setClickedListener(text -> {
                change();
            });
            list.setItemClickedListener((container, item, i, l) -> {
                Book book = bookList.get(i);
                RouterUtil.toAbility(ability, ".SearchAbility", book.getName());
            });
        }

        private void show() {
            show = true;
            tempList.addAll(bookList);
            provider.notifyDataChanged();
            parentProvider.notifyDataChanged();
        }
        private void hide() {
            show = false;
            tempList.clear();
            provider.notifyDataChanged();
            parentProvider.notifyDataChanged();
        }
        private void change() {
            show = !show;
            if (show) show(); else hide();
            text.setPressState(show);
        }

        void setParentProvider(BaseItemProvider provider) {
            parentProvider = provider;
        }
    }

    List<ListItem> rankingList;
    Ability ability;

    public RankingListProvider(List<ListItem> rankingList, Ability ability) {
        this.rankingList = rankingList;
        this.ability = ability;
    }

    @Override
    public int getCount() {
        return rankingList == null ? 0 : rankingList.size();
    }

    @Override
    public Object getItem(int i) {
        return  Optional.of(rankingList.get(i));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (rankingList.get(i).display) {
            return rankingList.get(i).component;
        }

        if (rankingList.get(i) != null) {
            component = rankingList.get(i).getComponent(ability);
            rankingList.get(i).addComponentListen();
            rankingList.get(i).setParentProvider(this);
        }
        return component;
    }
}
