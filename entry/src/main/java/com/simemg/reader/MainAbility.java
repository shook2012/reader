package com.simemg.reader;

import com.simemg.reader.fraction.main.BookshelfFraction;
import com.simemg.reader.fraction.main.GenresFraction;
import com.simemg.reader.fraction.main.RankingFraction;
import com.simemg.reader.utils.*;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.Window;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;

public class MainAbility extends FractionAbility {
    private int i = 0;
    private RadioContainer radios;
    private final Fraction[] fractions = {
        new BookshelfFraction(),
        new RankingFraction(),
        new GenresFraction()
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        requestPermissions();
        super.setUIContent(ResourceTable.Layout_ability_main);
        setStatusBarMaroon();

        initComponent();
        addComponentListen();
    }

    void requestPermissions() {
        if (verifySelfPermission(SystemPermission.WRITE_USER_STORAGE) != IBundleManager.PERMISSION_GRANTED) {
            LogUtil.info("未授权，申请权限");
            requestPermissionsFromUser(new String[] {SystemPermission.WRITE_USER_STORAGE}, 0);
        }
        else {
            LogUtil.info("已授权");
            StoreUtil.init(this);
            SpiderUtil.init();
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions == null || permissions.length == 0 || grantResults == null || grantResults.length == 0) {
            return;
        }
        if (requestCode == 0) {
            if (grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                LogUtil.info("已授权");
                StoreUtil.init(this);
                SpiderUtil.init();
            }
            else {
                LogUtil.info("被拒绝");
                terminateAbility();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (i != 0 )
            radios.mark(0);
        else
            super.onBackPressed();
    }


    void initComponent() {
        radios = (RadioContainer) findComponentById(ResourceTable.Id_radios);

        FractionScheduler scheduler = getFractionManager().startFractionScheduler();
        for (Fraction fraction:fractions) {
            scheduler = scheduler.add(ResourceTable.Id_layout, fraction);
        }
        scheduler.submit();
        showFraction(0);
    }

    void addComponentListen() {
        radios.setMarkChangedListener((radioContainer, i) -> showFraction(i));
    }

    void showFraction(int i) {
        this.i = i;
        FractionScheduler scheduler = getFractionManager().startFractionScheduler();
        int j = 0;
        for (Fraction fraction:fractions) {
            scheduler = (j == i) ? scheduler.show(fraction) : scheduler.hide(fraction);
            j++;
        }
        scheduler.submit();
    }

    void setStatusBarMaroon() {
        Window window = getWindow();
        window.setStatusBarColor(ColorUtil.get(this, ResourceTable.Color_maroon));
        window.setStatusBarVisibility(Component.VISIBLE);
    }
}
