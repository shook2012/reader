package com.simemg.reader.utils;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.global.configuration.DeviceCapability;
import ohos.multimodalinput.event.TouchEvent;

import java.util.HashMap;
import java.util.Map;


public class TouchUtil {
    private static final Map<Integer, Float[]> tempKeys = new HashMap<>();
    private static int height;
    private static int width;

    public static class Tag {
        private boolean up = false;
        private int sliderX = 0;
        private int sliderY = 0;
        private int componentPointX = 0;
        private int componentPointY = 0;
        private int touchPointX = 0;
        private int touchPointY = 0;
        private boolean click = false;

        public boolean isDown() {
            return up && sliderY == 1;
        }
        public boolean isUp() {
            return up && sliderY == -1;
        }
        public boolean isRight() {
            return up && sliderX == 1;
        }
        public boolean isLeft() {
            return up && sliderX == -1;
        }
        public boolean isClickMiddle() {
            return up && click && touchPointX == 0 && touchPointY == 0 && sliderX == 0 && sliderY == 0;
        }
        public boolean isDownNextPage() {
            return up && componentPointY == -1 && isDown();
        }
        public boolean isUpLastPage() {
            return up && componentPointY == 1 && isUp();
        }

        @Override
        public String toString() {
            return "Tag{" +
                    "up=" + up +
                    ", sliderX=" + sliderX +
                    ", sliderY=" + sliderY +
                    ", componentPointX=" + componentPointX +
                    ", componentPointY=" + componentPointY +
                    ", touchPointX=" + touchPointX +
                    ", touchPointY=" + touchPointY +
                    ", click=" + click +
                    '}';
        }
    }

    public static void init(Ability ability) {
        int density = ability.getResourceManager().getDeviceCapability().screenDensity / DeviceCapability.SCREEN_MDPI;
        height = ability.getContext().getResourceManager().getDeviceCapability().height * density / 3;  // 界面4分之一高度
        width = ability.getContext().getResourceManager().getDeviceCapability().width * density / 3;    // 界面4分之一宽度
    }

    public static Tag listen(Component component, TouchEvent touchEvent) {
        Tag tag = new Tag();
        int id = component.getId();
        float x = touchEvent.getPointerScreenPosition(0).getX();
        float y = touchEvent.getPointerScreenPosition(0).getY();
        float dx = component.getScrollValue(Component.AXIS_X);
        float dy = component.getScrollValue(Component.AXIS_Y);
        float time = (float)touchEvent.getOccurredTime();
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                tempKeys.put(id, new Float[]{x,y,dx,dy,time});
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                tag.up = true;
                Float[] p = tempKeys.get(id);

                if (x - p[0] > 100) tag.sliderX = 1;
                if (x - p[0] < -100) tag.sliderX = -1;
                if (y - p[1] > 100) tag.sliderY = -1;
                if (y - p[1] < -100) tag.sliderY = 1;
                if (dx == p[2]) tag.componentPointX = -1;
                if (dy == p[3]) tag.componentPointY = -1;
                if (dx == 0.0) tag.componentPointX = 1;
                if (dy == 0.0) tag.componentPointY = 1;
                if (time - p[4] < 500) tag.click = true;

                if (x < width) tag.touchPointX = -1;
                if (y < height) tag.touchPointY = -1;
                if (x > width * 3) tag.touchPointX = 1;
                if (y > height * 3) tag.touchPointY = 1;
                break;
        }
        return tag;
    }
}
