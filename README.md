<div>    
    <img style="border-radius:20px" src="./entry/src/main/resources/base/media/logo.png" width = "100" height = "100" alt="图片名称" align=center />
</div>

#### 描述
仿ios旧版笔趣阁app，新手作品，无法保证代码质量

##### 已实现功能

- 小说爬取
- 主题切换
- 小说朗读

##### 已知bug
- 网络慢或点击过快导致应用崩溃
- 排行榜第一次滑动失效

#### 应用生成
##### 编译环境
- DevEco Studio2.2 beta1 
- SDK 5
##### 本地运行

1. 克隆代码仓:

   ```shell
    git clone https://gitee.com/ctaolee/reader.git
   ```
   
2. 模拟器直接运行`entry`模块，真机运行需要配置[应用签名](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ide_debug_device-0000001053822404#section837891802519)

#### 软件截图
<div align="center">    
    <img src="./images/04.jpg" width = "200" alt="图片名称" align=center />
    <img src="./images/03.jpg" width = "200" alt="图片名称" align=center /><br><br>
    <img src="./images/02.jpg" width = "200" alt="图片名称" align=center />
    <img src="./images/01.jpg" width = "200" alt="图片名称" align=center />
</div>

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request