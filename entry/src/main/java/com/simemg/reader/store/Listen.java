package com.simemg.reader.store;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

@Entity(tableName = "listen")
public class Listen extends OrmObject {
    @PrimaryKey(autoGenerate = true)
    private Integer SettingID;
    private Integer Color;
    private Integer Light;
    private Integer TextSize;
    private Integer LineSize;
    private boolean NightMode;


}
