package com.simemg.reader.fraction.novel;

import com.simemg.reader.ResourceTable;
import com.simemg.reader.slice.NovelSlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

public class SettingFraction extends Fraction {
    private RadioContainer colorContainer;
    private final NovelSlice slice;
    private Component font1;
    private Component font2;
    private Component line1;
    private Component line2;
    private Component line3;
    private Text textSize;

    public SettingFraction(NovelSlice slice) {
        this.slice = slice;
    }

    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return scatter.parse(ResourceTable.Layout_fraction_novel_setting, container, false);
    }

    @Override
    protected void onActive() {
        super.onActive();

        initComponent();
        addComponentListen();
    }

    void initComponent() {
        colorContainer = (RadioContainer)getComponent().findComponentById(ResourceTable.Id_color_radios);
        colorContainer.mark(slice.getTheme_().getColor());

        font1 = getComponent().findComponentById(ResourceTable.Id_font1);
        font2 = getComponent().findComponentById(ResourceTable.Id_font2);
        line1 = getComponent().findComponentById(ResourceTable.Id_line1);
        line2 = getComponent().findComponentById(ResourceTable.Id_line2);
        line3 = getComponent().findComponentById(ResourceTable.Id_line3);
        textSize = (Text)getComponent().findComponentById(ResourceTable.Id_text_size);
        textSize.setText("" + slice.getTheme_().getTextSize());

        slice.updateTheme();

    }

    void addComponentListen() {
        colorContainer.setMarkChangedListener((radioContainer, i) -> {
            slice.getTheme_().setColor(i);
            slice.updateTheme();
        });

        font1.setClickedListener(component -> {
            slice.getTheme_().setTextSize( slice.getTheme_().getTextSize() - 1);
            slice.updateTheme();
            textSize.setText("" + slice.getTheme_().getTextSize());
        });
        font2.setClickedListener(component -> {
            slice.getTheme_().setTextSize( slice.getTheme_().getTextSize() + 1);
            slice.updateTheme();
            textSize.setText("" + slice.getTheme_().getTextSize());
        });

        line3.setClickedListener(component -> {
            slice.getTheme_().setLineSize(0);
            slice.updateTheme();
        });

        line2.setClickedListener(component -> {
            slice.getTheme_().setLineSize(2);
            slice.updateTheme();
        });

        line1.setClickedListener(component -> {
            slice.getTheme_().setLineSize(4);
            slice.updateTheme();
        });
    }


}
