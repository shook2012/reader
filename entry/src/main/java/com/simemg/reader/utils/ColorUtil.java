package com.simemg.reader.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class ColorUtil {

    /**
     *
     * @param context context
     * @param resourceId  资源id
     * @return int
     */
    public static int get(Context context, int resourceId) {
        int color = 0;
        try {
            color = context.getResourceManager().getElement(resourceId).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return color;
    }

    /**
     *
     * @param context context
     * @param resourceId  资源id
     * @return RbgColor
     */
    public static RgbColor getRbg(Context context, int resourceId) {
        int color = get(context, resourceId);
        if (color > 0) {
            int r = color & 0x00ff0000 >> 16;
            int g = color & 0x0000ff00 >> 8;
            int b = color & 0x0000ff;
            int a = color >> 24 & 0xf000000;
            return new RgbColor(r, g, b, a);
        } else {
            Color resultColor = new Color(color);
            return RgbColor.fromArgbInt(resultColor.getValue());
        }
    }

}
