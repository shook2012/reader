package com.simemg.reader.model;

import ohos.agp.components.Component;
import ohos.agp.render.BlurDrawLooper;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;

public class Shadow {
    private final Component component;
    private float shadowRadius = 18;
    private int color = Color.getIntColor("#4d000000");
    private float offsetX = 0;
    private float offsetY = 0;

    public Shadow(Component component) {
        this.component = component;
    }

    public static Shadow on(Component component) {
        return new Shadow(component);
    }

    public Shadow setColor(int color) {
        this.color = color;
        return this;
    }

    public Shadow setOffset(float x, float y) {
        offsetX = x;
        offsetY = y;
        return this;
    }

    public Shadow setShadowRadius(float shadowRadius) {
        this.shadowRadius = shadowRadius;
        return this;
    }

    public void execute() {
        this.component.addDrawTask((c, canvas) -> {
            Paint p = new Paint();
            BlurDrawLooper b = new BlurDrawLooper(shadowRadius, offsetX, offsetY, new Color(color));
            p.setBlurDrawLooper(b);
            RectFloat rect = new RectFloat(
                c.getPaddingLeft(),
                c.getPaddingTop(),
                (c.getWidth() - c.getPaddingRight()),
                (c.getHeight() - c.getPaddingBottom())
            );
            canvas.drawRoundRect(rect, 0,0, p);
        }, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
    }


}