package com.simemg.reader;

import com.simemg.reader.slice.NovelSlice;
import com.simemg.reader.utils.TouchUtil;
import ohos.aafwk.ability.AbilitySliceAnimator;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class NovelAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(NovelSlice.class.getName());

        TouchUtil.init(this);
        setAbilitySliceAnimator(new AbilitySliceAnimator().setDuration(0));
    }
}
