package com.simemg.reader.store;

import ohos.data.orm.OrmDatabase;
import ohos.data.orm.annotation.Database;

@Database(entities = {Spider.class, Book.class, Page.class, Theme.class}, version = 1)
public abstract class Store extends OrmDatabase {}