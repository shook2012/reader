package com.simemg.reader.store;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

@Entity(tableName = "page")
public class Page extends OrmObject {

    @PrimaryKey(autoGenerate = true)
    private Integer PageID;
    private Integer SpiderID;
    private Integer BookID;
    private Integer Num;
    private String Link;
    private String Title;
    private String Content;
    private Boolean Cache;

    public Integer getPageID() {
        return PageID;
    }

    public void setPageID(Integer pageID) {
        PageID = pageID;
    }

    public Integer getSpiderID() {
        return SpiderID;
    }

    public void setSpiderID(Integer spiderID) {
        SpiderID = spiderID;
    }

    public Integer getBookID() {
        return BookID;
    }

    public void setBookID(Integer bookID) {
        BookID = bookID;
    }

    public Integer getNum() {
        return Num;
    }

    public void setNum(Integer num) {
        Num = num;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Boolean getCache() {
        return Cache;
    }

    public void setCache(Boolean cache) {
        Cache = cache;
    }

}
