package com.simemg.reader.store;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

@Entity(tableName = "theme")
public class Theme extends OrmObject {
    @PrimaryKey(autoGenerate = true)
    private Integer SettingID;
    private Integer Color;
    private Integer Light;
    private Integer TextSize;
    private Integer LineSize;
    private boolean NightMode;

    public boolean isNightMode() {
        return NightMode;
    }

    public void setNightMode(boolean nightMode) {
        NightMode = nightMode;
    }

    public Integer getSettingID() {
        return SettingID;
    }

    public void setSettingID(Integer settingID) {
        SettingID = settingID;
    }

    public Integer getColor() {
        return Color;
    }

    public void setColor(Integer color) {
        Color = color;
    }

    public Integer getLight() {
        return Light;
    }

    public void setLight(Integer light) {
        Light = light;
    }

    public Integer getTextSize() {
        return TextSize;
    }

    public void setTextSize(Integer textSize) {
        TextSize = textSize;
    }

    public Integer getLineSize() {
        return LineSize;
    }

    public void setLineSize(Integer lineSize) {
        LineSize = lineSize;
    }
}
