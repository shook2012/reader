package com.simemg.reader.utils;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import java.io.Serializable;

public class RouterUtil {

    public static void toAbility(Ability slice, String name) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.simemg.reader")
                .withAbilityName(name)
                .build();
        intent.setOperation(operation);
        slice.startAbility(intent);
    }

    public static void toAbility(Ability ability, String name, Serializable data) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.simemg.reader")
                .withAbilityName(name)
                .build();
        intent.setOperation(operation);
        intent.setParam("data", data);
        ability.startAbility(intent);
    }

    public static Object getData(Intent intent) {
        return intent.getSerializableParam("data");
    }

    public static void toSlice(AbilitySlice abilitySlice, AbilitySlice toSlice, Serializable data) {
        Intent intent = new Intent();
        intent.setParam("data", data);
        abilitySlice.present(toSlice, intent);
    }

}
