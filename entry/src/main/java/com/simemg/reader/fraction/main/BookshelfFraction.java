package com.simemg.reader.fraction.main;

import com.simemg.reader.ResourceTable;
import com.simemg.reader.model.Shadow;
import com.simemg.reader.store.Book;
import com.simemg.reader.utils.*;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

public class BookshelfFraction extends Fraction {
    private TableLayout tableLayout;
    private Button search;
    private Button edit;
    private List<Component> deleteList;

    private final List<Book> bookList = new ArrayList<>();

    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return scatter.parse(ResourceTable.Layout_fraction_main_bookshelf, container, false);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        initComponent();
        addComponentListen();
    }

    @Override
    protected void onActive() {
        super.onActive();
        loadData();
    }


    void initComponent(){
        tableLayout = (TableLayout) getComponent().findComponentById(ResourceTable.Id_table_layout);
        search = (Button) getComponent().findComponentById(ResourceTable.Id_search);
        edit = (Button) getComponent().findComponentById(ResourceTable.Id_edit);
    }

    void addComponentListen() {
        search.setClickedListener(component -> RouterUtil.toAbility(getFractionAbility(), ".SearchAbility"));
        edit.setClickedListener(component -> {
            selectAndDelete();
        });
    }

    void loadData() {
        List<Book> newList = StoreUtil.getAllStoreBook();
        deleteList = new ArrayList<>();
        for (Book book:newList) if (!bookList.contains(book)) {
            bookList.add(book);
            tableLayoutAddBook(book);
        }
    }

    void tableLayoutAddBook(Book book) {
        // init
        Component bookItem = LayoutScatter.getInstance(getFractionAbility())
                .parse(ResourceTable.Layout_component_store_book, null, false);
        Text name = (Text) bookItem.findComponentById(ResourceTable.Id_name);
        Image image = (Image) bookItem.findComponentById(ResourceTable.Id_img);
        Component layout = bookItem.findComponentById(ResourceTable.Id_img_layout);
        Component delete = bookItem.findComponentById(ResourceTable.Id_delete);
        name.setText(book.getName());
        RequestUtil.LoadImg(book.getImg()).setContext(getFractionAbility()).setImage(image).execute();
        tableLayout.addComponent(bookItem);
        Shadow.on(layout).execute();

        // add listen
        bookItem.setClickedListener(component -> RouterUtil.toAbility(getFractionAbility(), ".NovelAbility", book.getBookID()));
        bookItem.setLongClickedListener(component -> selectAndDelete());
        delete.setClickedListener(component -> {
            StoreUtil.bookStoreDel(book);
            tableLayout.removeComponent(bookItem);
        });

        deleteList.add(delete);
    }

    void selectAndDelete() {
        for (Component delete:deleteList) {
            delete.setVisibility(Component.VISIBLE);
        }
    }
}
