package com.simemg.reader.fraction.novel;

import com.simemg.reader.ResourceTable;
import com.simemg.reader.slice.IndexSlice;
import com.simemg.reader.slice.ListenSlice;
import com.simemg.reader.slice.NovelSlice;
import com.simemg.reader.store.Page;
import com.simemg.reader.utils.RequestUtil;
import com.simemg.reader.utils.RouterUtil;
import com.simemg.reader.utils.SpiderUtil;
import com.simemg.reader.utils.StoreUtil;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.components.element.VectorElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ohos.agp.animation.Animator.INFINITE;

public class MenuFraction extends Fraction {
    private boolean isNight = false;
    private boolean isDown = false;
    private final NovelSlice slice;

    private Component left;
    private Component right;
    private Component menu;
    private Component night_mode;
    private Component setting;
    private Component listen;
    private Component night_image;
    private Component down_image;
    private Component down;
    private Text night_text;
    private Text down_text;
    private Text title;
    private ProgressBar process;

    public MenuFraction(NovelSlice slice) {
        this.slice = slice;
    }

    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return scatter.parse(ResourceTable.Layout_fraction_novel_menu, container, false);
    }

    @Override
    protected void onActive() {
        super.onActive();

        initComponent();
        addComponentListen();
    }

    void initComponent() {
        menu = getComponent().findComponentById(ResourceTable.Id_menu);
        left = getComponent().findComponentById(ResourceTable.Id_left);
        right = getComponent().findComponentById(ResourceTable.Id_right);
        title = (Text)getComponent().findComponentById(ResourceTable.Id_title);
        setting = getComponent().findComponentById(ResourceTable.Id_setting);
        process = (ProgressBar) getComponent().findComponentById(ResourceTable.Id_progress);
        night_mode = getComponent().findComponentById(ResourceTable.Id_night_mode);
        listen = getComponent().findComponentById(ResourceTable.Id_listen);
        down = getComponent().findComponentById(ResourceTable.Id_download);

        night_image = night_mode.findComponentById(ResourceTable.Id_img_night);
        night_text = (Text) night_mode.findComponentById(ResourceTable.Id_text_night);
        down_image = down.findComponentById(ResourceTable.Id_img_download);
        down_text = (Text) down.findComponentById(ResourceTable.Id_text_download);

        updateProgress();
        if (slice.getTheme_().isNightMode()) {
            isNight = true;
            updateNightMode();
        }
        if (slice.getBook().getDown()) {
            down_image.setBackground(new VectorElement(getFractionAbility(), ResourceTable.Graphic_svg_download_done));
            down_text.setText("已缓存");
        }
    }

    void addComponentListen() {
        menu.setClickedListener(component -> {
            RouterUtil.toSlice(slice, new IndexSlice(), slice.getBook().getBookID());
        });
        left.setClickedListener(component -> {
            slice.onLast();
            updateProgress();
        });
        right.setClickedListener(component -> {
            slice.onNext();
            updateProgress();
        });
        setting.setClickedListener(component -> {
            slice.onSetting();
        });
        night_mode.setClickedListener(component -> {
            slice.getTheme_().setNightMode(!isNight);
            slice.updateTheme();
            isNight = !isNight;
            updateNightMode();
        });
        listen.setClickedListener(component -> {
            RouterUtil.toSlice(slice, new ListenSlice(), slice.getBook().getBookID());
        });
        down.setClickedListener(component -> {
            if (isDown || slice.getBook().getDown()) return;
            isDown = true;
            down_image.setBackground(new VectorElement(getFractionAbility(), ResourceTable.Graphic_svg_loading));
            AnimatorProperty animator = down_image.createAnimatorProperty();
            animator.rotate(360).setDuration(2000).setLoopedCount(INFINITE).start();
            down_text.setText("缓存中");

            int start = slice.getBook().getNowNum();
            int stop = slice.getBook().getMaxNum();
            List<RequestUtil.DoSomething> list = new ArrayList<>();
            Map<Integer, Page> pageMap = new HashMap<>();
            for (int i = start; i< stop; i++) {
                Page page = StoreUtil.searchPage(slice.getBook(), i);
                list.add(SpiderUtil.getSearchBookPage(page));
                pageMap.put(i, page);
            }
            RequestUtil.Do(list)
                    .setContext(getFractionAbility())
                    .onSuccess((RequestUtil.CallBackMapObject) (result)->{
                        for (Integer j:result.keySet()) if(result.get(j) != null) {
                            String content = (String) result.get(j);
                            StoreUtil.savePage(pageMap.get(j + start), content);
                        }
                        isDown = false;
                        animator.end();
                        down_image.setBackground(new VectorElement(getFractionAbility(), ResourceTable.Graphic_svg_download_done));
                        down_text.setText("已缓存");
                        StoreUtil.setBookIsDown(slice.getBook());
                    })
                    .execute();

        });
    }

    public void updateProgress() {
        if (process == null || slice.getBook() == null || slice.getBook().getMaxNum() == null) return;

        process.setProgressValue(slice.getBook().getNowNum());
        process.setMaxValue(slice.getBook().getMaxNum());
        title.setText(slice.getPage().getTitle());
    }

    public void updateNightMode() {
        night_image.setPressState(isNight);
        night_text.setText(isNight ? "白天" : "夜间");
    }

}