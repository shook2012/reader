package com.simemg.reader.provider;

import com.simemg.reader.ResourceTable;
import com.simemg.reader.store.Page;
import com.simemg.reader.utils.ColorUtil;
import com.simemg.reader.utils.LogUtil;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

import java.util.List;
import java.util.Optional;

public class PageListProvider extends BaseItemProvider {
    private final List<Page> pageList;
    private final Context context;
    private Integer num;

    public PageListProvider(List<Page> pageList, Context context) {
        this.pageList = pageList;
        this.context = context;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public int getCount() {
        return pageList == null ? 0 : pageList.size();
    }

    @Override
    public Object getItem(int i) {
        return  Optional.of(this.pageList.get(i));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_component_page, null, false);
        if (!(component instanceof ComponentContainer)) {
            return null;
        }
        if (pageList.get(i).getTitle() != null) {
            Text title = (Text)component.findComponentById(ResourceTable.Id_title);
            title.setText(pageList.get(i).getTitle());
            if (pageList.get(i).getNum().equals(num)) {
                title.setTextColor(new Color(ColorUtil.get(context, ResourceTable.Color_maroon)));
            }

        }
        if (pageList.get(i).getCache() != null) {
            if (pageList.get(i).getCache()) {
                Component cache = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_component_cache, null, false);
                DirectionalLayout layout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_title_layout);
                layout.addComponent(cache);
            }
        }
        return component;
    }
}
