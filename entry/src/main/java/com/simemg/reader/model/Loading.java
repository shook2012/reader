package com.simemg.reader.model;


import com.simemg.reader.ResourceTable;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RoundProgressBar;

import static ohos.agp.animation.Animator.INFINITE;

public class Loading  {
    private static final int LOADING = 0x00023;
    private static AnimatorProperty loadAnimator;

    public static void show(ComponentContainer layout) {
        Component component = LayoutScatter.getInstance(layout.getContext())
                .parse(ResourceTable.Layout_component_loading, null, false);
        component.setId(LOADING);
        RoundProgressBar roundProgressBar = (RoundProgressBar)component.findComponentById(ResourceTable.Id_progress);
        loadAnimator = roundProgressBar.createAnimatorProperty();
        loadAnimator.rotate(360).setLoopedCount(INFINITE);
        layout.addComponent(component);
        loadAnimator.start();
    }

    public static void hide(ComponentContainer layout) {
        layout.removeComponentById(LOADING);
        loadAnimator.stop();
    }
}
