package com.simemg.reader.store;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "book")
public class Book extends OrmObject implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private Integer BookID;
    private Integer SpiderID;
    private String Link;
    private String Name;
    private String Author;
    private String Img;
    private String Info;
    private String LastPage;
    private Integer NowNum;
    private Integer MaxNum;
    private Boolean InStore;
    private Boolean Down;
    private long LastTime;

    public Integer getBookID() {
        return BookID;
    }

    public void setBookID(Integer bookID) {
        BookID = bookID;
    }

    public Integer getSpiderID() {
        return SpiderID;
    }

    public void setSpiderID(Integer spiderID) {
        SpiderID = spiderID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getImg() {
        return Img;
    }

    public void setImg(String img) {
        Img = img;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getLastPage() {
        return LastPage;
    }

    public void setLastPage(String lastPage) {
        LastPage = lastPage;
    }

    public Integer getNowNum() {
        return NowNum;
    }

    public void setNowNum(Integer nowNum) {
        NowNum = nowNum;
    }

    public Boolean getInStore() {
        return InStore;
    }

    public void setInStore(Boolean inStore) {
        InStore = inStore;
    }

    public long getLastTime() {
        return LastTime;
    }

    public void setLastTime(long lastTime) {
        LastTime = lastTime;
    }

    public Integer getMaxNum() {
        return MaxNum;
    }

    public void setMaxNum(Integer maxNum) {
        MaxNum = maxNum;
    }

    public Boolean getDown() {
        if (Down == null) return false;
        return Down;
    }

    public void setDown(Boolean down) {
        Down = down;
    }

    @Override
    public String toString() {
        return "Book{" +
                "BookID=" + BookID +
                ", SpiderID=" + SpiderID +
                ", Name='" + Name + '\'' +
                ", Author='" + Author + '\'' +
                ", Img='" + Img + '\'' +
                ", Info='" + Info + '\'' +
                ", Link='" + Link + '\'' +
                ", LastPage='" + LastPage + '\'' +
                ", PageNum=" + NowNum +
                ", InStore=" + InStore +
                ", LastTime=" + LastTime +
                ", MaxNum=" + MaxNum +
                '}';
    }
}
