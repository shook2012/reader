package com.simemg.reader.fraction.main;

import com.simemg.reader.ResourceTable;
import com.simemg.reader.provider.RankingListProvider;
import com.simemg.reader.store.Book;
import com.simemg.reader.utils.LogUtil;
import com.simemg.reader.utils.RequestUtil;
import com.simemg.reader.utils.RouterUtil;
import com.simemg.reader.utils.SpiderUtil;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;


public class RankingFraction extends Fraction {

    private final List<RankingListProvider.ListItem> rankingList = new ArrayList<>();
    private RankingListProvider rankingListProvider;

    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return scatter.parse(ResourceTable.Layout_fraction_main_ranking, container, false);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        initComponent();
        loadData();
        addComponentListen();
    }

    void initComponent() {
        ListContainer listContainer = (ListContainer) getComponent().findComponentById(ResourceTable.Id_list);
        rankingListProvider = new RankingListProvider(rankingList, getFractionAbility());
        listContainer.setItemProvider(rankingListProvider);
    }

    void loadData() {
        RequestUtil.Do(SpiderUtil.getSearchQiDian()).onSuccess((RequestUtil.CallBackObject) (result)->{
            List<Book> bookList = (List<Book>)result;
            if (bookList != null) {
                LogUtil.info("已获取起点月票榜");
                rankingList.add(new RankingListProvider.ListItem("起点月票榜", bookList));
                rankingListProvider.notifyDataChanged();
            }
        }).execute();

        RequestUtil.Do(SpiderUtil.getSearchBaidu()).onSuccess((RequestUtil.CallBackObject) (result)->{
            List<Book> bookList = (List<Book>)result;
            if (bookList != null) {
                LogUtil.info("已获取百度热搜榜");
                rankingList.add(new RankingListProvider.ListItem("百度热搜榜", bookList));
                rankingListProvider.notifyDataChanged();
            }
        }).execute();

        RequestUtil.Do(SpiderUtil.getSearchZongheng()).onSuccess((RequestUtil.CallBackObject) (result)->{
            List<Book> bookList = (List<Book>)result;
            if (bookList != null) {
                LogUtil.info("已获取纵横月票榜");
                rankingList.add(new RankingListProvider.ListItem("纵横月票榜", bookList));
                rankingListProvider.notifyDataChanged();
            }
        }).execute();

    }

    void addComponentListen() {}
}