package com.simemg.reader;

import com.simemg.reader.model.Loading;
import com.simemg.reader.store.Book;
import com.simemg.reader.store.Page;
import com.simemg.reader.utils.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.Window;

import java.util.List;

public class InfoAbility extends Ability {
    private List<Page> pageList;
    private Book book;

    private StackLayout layout;

    private Button addStore;
    private Button read;
    private Text name;
    private Text info;
    private Image image;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_info);
        setStatusBarMaroon();

        initComponent();
        loadData(intent);
        addComponentListen();
    }

    void initComponent() {
        addStore = (Button) findComponentById(ResourceTable.Id_add_store);
        read = (Button) findComponentById(ResourceTable.Id_read);
        name = (Text) findComponentById(ResourceTable.Id_name);
        info = (Text) findComponentById(ResourceTable.Id_info);
        image = (Image) findComponentById(ResourceTable.Id_img);

        layout = (StackLayout)findComponentById(ResourceTable.Id_load_layout);
    }

    void loadData(Intent intent) {
        book = (Book) RouterUtil.getData(intent);
        book = StoreUtil.updateOrInsertBook(book);
        Loading.show(layout);
        RequestUtil.Do(SpiderUtil.getSearchBookPageList(book))
                .onSuccess((RequestUtil.CallBackObject)(result)->{
                    pageList = (List<Page>) result;
                    LogUtil.info("已获取到章节数:" + pageList.size());
                    Loading.hide(layout);
                })
                .execute();

        if (book.getName() != null) name.setText(book.getName());
        if (book.getImg() != null) RequestUtil.LoadImg(book.getImg()).setContext(this).setImage(image).execute();
        if (book.getInfo() != null) info.setText(book.getInfo());
    }

    void addComponentListen() {
        addStore.setClickedListener(component -> {
            LogUtil.info("将书加入书架:"+book.getName());
            StoreUtil.addBookStore(book);
            StoreUtil.savePageList(book, pageList);
        });
        read.setClickedListener(component -> {
            LogUtil.info("阅读，章节:"+book.getNowNum());
            StoreUtil.savePageList(book, pageList);
            getMainTaskDispatcher().delayDispatch(()->{
                RouterUtil.toAbility(this, ".NovelAbility", book.getBookID());
            }, 500);
        });
    }

    void setStatusBarMaroon() {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_maroon).getColor();
            Window window = getWindow();
            window.setStatusBarColor(color);
            window.setStatusBarVisibility(Component.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
