package com.simemg.reader.utils;

import com.simemg.reader.store.*;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmObject;
import ohos.data.orm.OrmPredicates;
import ohos.utils.zson.ZSONArray;

import java.util.ArrayList;
import java.util.List;

public class StoreUtil {
    private static OrmContext context;

    public static void init(Context appContext) {
        LogUtil.info("本地对象型数据库初始化");
        DatabaseHelper helper = new DatabaseHelper(appContext);
        context = helper.getOrmContext("Store", "Store.db", Store.class);
    }

    public static List<Spider> getAllSpider() {
        OrmPredicates query = context.where(Spider.class);
        return context.query(query);
    }

    public static void batchInsertSpider(String result) {
        ZSONArray settings = ZSONArray.stringToZSONArray(result);
        int size = settings.size();
        for (int i=0;i<size;i++) {
            List<String> setting = (List<String>)settings.get(i);
            Spider spider = new Spider();
            spider.setSpiderID(Integer.parseInt(setting.get(0)));
            spider.setHostUrl(setting.get(1));
            spider.setSearchUrl(setting.get(2));
            spider.setBookSelect(setting.get(3));
            spider.setIndexUrlSelect(setting.get(4));
            spider.setNameSelect(setting.get(5));
            spider.setAuthorSelect(setting.get(6));
            spider.setImgSelect(setting.get(7));
            spider.setInfoSelect(setting.get(8));
            spider.setLastPageSelect(setting.get(9));
            spider.setPageSelect(setting.get(10));
            spider.setContentSelect(setting.get(11));
            spider.setInrInfoSelect(setting.get(12));
            spider.setInrImgSelect(setting.get(13));
            context.insert(spider);
        }
        context.flush();
        List<Spider> spiders = StoreUtil.getAllSpider();
        for (Spider spider : spiders) {
            SpiderUtil.spiderUtilMap.put(spider.getSpiderID(), new SpiderUtil(spider));
        }
    }

    public static Book updateOrInsertBook(Book book) {
        OrmPredicates query = context.where(Book.class);
        query.equalTo("Link", book.getLink());
        Book result;
        List<Book> books = context.query(query);
        if (books.size() == 0) {
            book.setNowNum(0);
            book.setLastTime(System.currentTimeMillis());
            context.insert(book);
            result = book;
        }
        else {
            result = books.get(0);
            result.setLastPage(book.getLastPage());
            result.setLastTime(System.currentTimeMillis());
            context.update(result);
        }
        context.flush();
        return result;
    }

    public static void bookStoreDel(Book book) {
        book.setInStore(false);
        context.update(book);
        context.flush();
    }

    public static void addBookStore(Book book) {
        book.setInStore(true);
        context.update(book);
        context.flush();
    }

    public static void savePageList(Book book, List<Page> pageList) {
        Integer bookID = book.getBookID();

        if (book.getMaxNum() == null) {
            if (pageList == null || pageList.size() == 0) return ;
            for (Page page:pageList) {
                context.insert(page);
            }
            book.setMaxNum(pageList.size());
            context.update(book);
            context.flush();
        }
        else {
            int size = book.getMaxNum();
            int aSize = pageList.size();
            if (size != aSize) {
                if (aSize > size) {
                    for (int i = size + 1; i< aSize; i ++) {
                        context.insert(pageList.get(i));
                    }
                }
                else {
                    OrmPredicates query = context.where(Page.class);
                    query.equalTo("BookID", bookID);
                    List<OrmObject> queryResult = context.query(query);
                    for (OrmObject o:queryResult) {
                        context.delete(o);
                    }
                    for (Page page : pageList) {
                        context.insert(page);
                    }
                }
            }
            else {
                LogUtil.info("无需更新章节");
            }
        }
        context.flush();
    }

    public static Book searchBook(Integer BookID) {
        OrmPredicates query = context.where(Book.class);
        query.equalTo("BookID", BookID);
        List<Book> books = context.query(query);
        return books.get(0);
    }


    public static Page readPage(Book book, int num) {
        book.setNowNum(num);
        context.update(book);
        context.flush();

        if (book.getInStore() == null) {
            OrmPredicates query = context.where(Page.class);
            query.equalTo("BookID", book.getBookID());
            query.equalTo("Cache", true);
            List<OrmObject> queryResult = context.query(query);
            if (queryResult.size() >= 10) {
                book.setInStore(true);
                context.update(book);
                context.flush();
            }
        }

        return searchPage(book, num);
    }

    public static Page searchPage(Book book, int num) {
        OrmPredicates query = context.where(Page.class);
        query.equalTo("BookID", book.getBookID());
        query.equalTo("Num", num);
        List<OrmObject> queryResult = context.query(query);
        if (queryResult.size() == 0) return null;
        return (Page)queryResult.get(0);
    }


    public static void savePage(Page page, String content) {
        page.setContent(content);
        page.setCache(true);
        context.update(page);
        context.flush();
    }

    public static List<Page> searchPageList(Book book) {
        OrmPredicates query = context.where(Page.class);
        query.equalTo("BookID", book.getBookID());
        return context.query(query);
    }

    public static List<Book> getAllStoreBook() {
        if (context == null) return new ArrayList<>();
        OrmPredicates query = context.where(Book.class);
        query.equalTo("InStore", true);
        return context.query(query);
    }

    public static Theme getTheme() {
        OrmPredicates query = context.where(Theme.class);
        List<Theme> result = context.query(query);
        Theme theme;
        if (result.size() == 0) {
            theme = new Theme();
            theme.setNightMode(false);
            theme.setColor(0);
            theme.setLight(0);
            theme.setTextSize(16);
            theme.setLineSize(0);
            context.insert(theme);
            context.flush();
        }
        else {
            theme = result.get(0);
        }
        return theme;
    }

    public static void updateTheme(Theme theme) {
        context.update(theme);
        context.flush();
    }

    public static void setBookIsDown(Book book) {
        book.setDown(true);
        context.update(book);
        context.flush();
    }
}
